import org.apache.spark.SparkContext
import org.apache.spark.SparkConf

import kifimining.Crawler._
import org.apache.spark.rdd.RDD

object KifiAnalyzer {

   def main(args: Array[String]) {
     if (args.length < 1) {
       System.err.println("Usage: KifiAnalyzer <exported_file_path>")
       System.exit(1)
     }

    val bookmarksFile = args(0)

	val conf = new SparkConf().setAppName("Kifi Analyzer").set("spark.ui.port","4141")
	val sc = new SparkContext(conf)

	val linksRDD = loadLinks(sc, bookmarksFile)
	val articlesRDD = linksRDD.map(Crawler.bodyText).cache

     

   }
   
 }

