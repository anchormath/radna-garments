package kifimining

object Crawler {

	import org.apache.spark.SparkContext
	import org.apache.spark.rdd.RDD
	import org.jsoup.Jsoup
	import org.jsoup.nodes.Element
	import scala.collection.JavaConverters._
	
	def loadLinks(sc:SparkContext, filePath: String): RDD[String] = {
		val bkmarkFile = new java.io.File("/home/adminuser/workspace/KifiMining/keepExports.html")

		val bkmarkDoc = Jsoup.parse(bkmarkFile, "UTF-8")

		val linkElems : scala.collection.Iterator[org.jsoup.nodes.Element] = bkmarkDoc.select("a[href]").iterator.asScala

		val linkUrls = linkElems.map(_.attr("href")).toArray

		sc.parallelize(linkUrls)

		}

	def bodyText(url:String): String = { 
		val pageDoc = Jsoup.connect(url).get
		pageDoc.body.text
	}
}